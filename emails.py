import os
from cryptography.fernet import Fernet, InvalidToken
from flask import render_template
from flask_mail import Mail, Message

mail = Mail()


def send_email(token, message, body, html, username, email):
    try:
        # Create a Message object
        msg = Message(message,
                      sender=os.getenv('MAIL_USERNAME'),
                      recipients=[email],
                      body=body,
                      html=render_template(html, username=username, token=token))
        mail.send(msg)
        return "Email sent successfully!"
    except Exception as e:
        return f"An error occurred: {e}"


def mail_password():
    try:
        # Load the key from a secure place
        key = os.getenv('key')
        fernet = Fernet(key)
        encrypted_password = os.getenv('encrypted_password')
        decrypted_password = fernet.decrypt(encrypted_password.encode()).decode()

        return decrypted_password
    except InvalidToken:
        print("Invalid key - Decryption failed.")
    except Exception as e:
        print(f"An error occurred: {e}")
    return None
