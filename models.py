from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.dialects.mssql import information_schema
from sqlalchemy.orm import joinedload
# psysopg2
from datetime import datetime

# configuration
# create DB instance
db = SQLAlchemy()

# Todo : split users to customers and employees, add emp_id to fact orders.
# Todo: check dim sub group not distinct, add product cost.
# Todo: customers- add address, phone numbers check for api that checks addresses and etc.


def check_table(table_name):
    return information_schema.tables.query.filter_by(table_name == 'table_name').first()


# define tables structure as classes

class Employees(db.Model):
    __tablename__ = 'employees'
    employee_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True, nullable=False)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    phone = db.Column(db.String(10), nullable=False)
    email = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(50), nullable=False)
    is_admin = db.Column(db.Integer, nullable=False, default=0)  # 1 yes, 0 no.
    default_branch_id = db.Column(db.Integer, nullable=False)

    orders = db.relationship('Orders', backref='employee', lazy='select')

    def to_dict(self):
        """Return a dictionary representation of the employee, excluding the password."""
        employee = db.session.query(Employees).options(
            joinedload(Employees.orders)
        ).get(self.employee_id)

        return {
            'employee_id': employee.employee_id,
            'username': employee.username,
            'first_name': employee.first_name,
            'last_name': employee.last_name,
            'email': employee.email,
            'is_admin': employee.is_admin,
            'orders': [order.to_dict() for order in employee.orders]
        }


class Customers(db.Model):
    __tablename__ = 'customers'
    customer_id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(50), unique=True, nullable=False)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    password = db.Column(db.String(50), nullable=False)
    phone = db.Column(db.String(10), nullable=False)
    email = db.Column(db.String(50), unique=True, nullable=False)
    city = db.Column(db.String(50), nullable=False)
    street = db.Column(db.String(50), nullable=False)
    postal_code = db.Column(db.String(10), nullable=False)

    orders = db.relationship('Orders', backref='customer', lazy='select')

    def to_dict(self):
        """Return a dictionary representation of the user, excluding the password."""
        customer = db.session.query(Customers).options(
            joinedload(Customers.orders)
        ).get(self.customer_id)

        return {
            'employee_id': customer.customer_id,
            'username': customer.username,
            'first_name': customer.first_name,
            'last_name': customer.last_name,
            'email': customer.email,
            'orders': [order.to_dict() for order in customer.orders]
        }


# dimension, holds all product details
class Products(db.Model):
    __tablename__ = 'products'
    product_id = db.Column(db.Integer, primary_key=True)
    product_name = db.Column(db.String(50), nullable=False)
    product_desc = db.Column(db.Text)  # Descriptive text about the product
    product_subgroup_id = db.Column(db.Integer, db.ForeignKey(
        'productSubGroup.product_subgroup_id'))  # Foreign key to ProductSubGroup
    product_image = db.Column(db.String(50))  # URL or path to the product image
    product_price = db.Column(db.Float)  # Price of the product
    # quantity_available = db.Column(db.Integer) -- if you want to have here the current stock

    inventory_items = db.relationship('Inventory', backref='product', lazy=True)  # Relationship to Inventory
    orderdetails = db.relationship('OrderDetails', backref='product', lazy=True)  # Relationship to orderdetails

    def to_dict(self):
        product = db.session.query(Products).options(
            joinedload(Products.inventory_items),
            joinedload(Products.orderdetails)
        ).get(self.product_id)

        return {
            'product_id': product.product_id,
            'product_name': product.product_name,
            'product_desc': product.product_desc,
            'product_image': product.product_image,
            'product_price': product.product_price,
            'product_subgroup_id': product.product_subgroup_id,
            'inventory_items': [inventory.to_dict() for inventory in product.inventory_items],
            'orderdetails': [detail.to_dict() for detail in product.orderdetails]
        }


class ProductSubGroup(db.Model):
    __tablename__ = 'productSubGroup'
    product_subgroup_id = db.Column(db.Integer, primary_key=True)
    product_subgroup_name = db.Column(db.String(50), nullable=False)
    product_subgroup_image = db.Column(db.String(50), nullable=False)
    product_group_id = db.Column(db.Integer, db.ForeignKey(
        'productGroup.product_group_id'))  # Group or category of the product, Foreign key to ProductGroup

    # This will contain all products in this group
    products = db.relationship('Products', backref='product_subgroup', lazy=True)

    def to_dict(self):
        """
        Return a dictionary representation of a product subgroup, including detailed
        information about each product within this subgroup using joinedload for optimization.
        """
        subgroup = db.session.query(ProductSubGroup).options(
            joinedload(ProductSubGroup.products)
        ).get(self.product_subgroup_id)

        return {
            'product_subgroup_id': subgroup.product_subgroup_id,
            'product_subgroup_name': subgroup.product_subgroup_name,
            'product_subgroup_image': subgroup.product_subgroup_image,
            'product_group_id': subgroup.product_group_id,
            'products': [product.to_dict() for product in subgroup.products]
        }


class ProductGroup(db.Model):
    __tablename__ = 'productGroup'
    product_group_id = db.Column(db.Integer, primary_key=True)
    product_group_name = db.Column(db.String(50),  nullable=False)
    product_group_image = db.Column(db.String(50),  nullable=False)

    # This will contain all products in this group
    productsSub = db.relationship('ProductSubGroup', backref='product_group', lazy=True)

    def to_dict(self):
        """
        Return a dictionary representation of a product group, including all nested
        subgroups and the products within those subgroups, using joinedload for optimization.
        """
        print("Converting ProductGroup object to dictionary...")

        group = db.session.query(ProductGroup).options(
            joinedload(ProductGroup.productsSub).joinedload(ProductSubGroup.products)
        ).get(self.product_group_id)

        print("ProductGroup query result:", group)

        data = {
            'product_group_id': group.product_group_id,
            'product_group_name': group.product_group_name,
            'product_group_image': group.product_group_image,
            'subgroups': [subgroup.to_dict() for subgroup in group.productsSub]
        }

        print("Dictionary representation of ProductGroup:", data)
        return data
    # def to_dict(self):
    #     """
    #     Return a dictionary representation of a product group, including all nested
    #     subgroups and the products within those subgroups, using joinedload for optimization.
    #     """
    #     group = db.session.query(ProductGroup).options(
    #         joinedload(ProductGroup.productsSub).joinedload(ProductSubGroup.products)
    #     ).get(self.product_group_id)
    #
    #     return {
    #         'product_group_id': group.product_group_id,
    #         'product_group_name': group.product_group_name,
    #         'product_group_image': group.product_group_image,
    #         'subgroups': [subgroup.to_dict() for subgroup in group.productsSub]
    #     }


# dimension, holds all Branches details
class Branches(db.Model):
    __tablename__ = 'branches'
    branch_id = db.Column(db.Integer, primary_key=True)
    branch_name = db.Column(db.String(50), nullable=False)  # Name of the branch
    branch_location = db.Column(db.Text)  # Physical location or address of the branch
    branch_type = db.Column(db.Integer)  # Type of branch, e.g., 1 for online, 2 for physical store

    inventory_items = db.relationship('Inventory', backref='branch', lazy=True)  # Relationship to Inventory
    orders = db.relationship('Orders', backref='branch', lazy=True)  # Relationship to Orders

    def to_dict(self):
        """
        Return a dictionary representation of a branch, using joinedload for efficient
        data loading of its inventory items and orders.
        """
        branch = db.session.query(Branches).options(
            joinedload(Branches.inventory_items),
            joinedload(Branches.orders)
        ).get(self.branch_id)

        return {
            'branch_id': branch.branch_id,
            'branch_name': branch.branch_name,
            'branch_location': branch.branch_location,
            'branch_type': branch.branch_type,
            'inventory_items': [item.to_dict() for item in branch.inventory_items],
            'orders': [order.to_dict() for order in branch.orders]
        }


# Fact order transaction headers
class Orders(db.Model):
    __tablename__ = 'orders'
    order_id = db.Column(db.Integer, primary_key=True)
    order_date = db.Column(db.DateTime, default=datetime.utcnow)  # Date and time when the order was placed
    branch_id = db.Column(db.Integer, db.ForeignKey('branches.branch_id'))  # Foreign key to Branches
    employee_id = db.Column(db.Integer, db.ForeignKey('employees.employee_id'))  # Foreign key to employee
    customer_id = db.Column(db.Integer, db.ForeignKey('customers.customer_id'))  # Foreign key to customer_id
    status_id = db.Column(db.Integer)  # Status of the order, e.g., 1 for finished, 2 for ongoing

    orderdetails = db.relationship('OrderDetails', backref='order', lazy=True)  # Relationship to orderdetails

    def to_dict(self):
        """
            Return a dictionary representation of an order, including detailed information
            about its order details and related user and branch information, using joinedload
            for performance optimization.
            """
        order = db.session.query(Orders).options(
            joinedload(Orders.orderdetails),
            joinedload(Orders.customer),
            joinedload(Orders.employee),
            joinedload(Orders.branch)
        ).get(self.order_id)

        return {
            'order_id': order.order_id,
            'order_date': order.order_date.strftime('%Y-%m-%d %H:%M:%S'),
            'branch': {
                'branch_id': order.branch.branch_id,
                'branch_name': order.branch.branch_name
            } if order.branch else None,
            'employee': {
                'employee_id': order.employee.employee_id,
                'username': order.employee.username,
                'email': order.employee.email
            } if order.employee else None,
            'customer': {
                'customer_id': order.customer.customer_id,
                'username': order.customer.username,
                'email': order.customer.email
            } if order.employee else None,
            'status_id': order.status_id,
            'orderdetails': [detail.to_dict() for detail in order.orderdetails]
        }


class OrderDetails(db.Model):
    __tablename__ = 'orderdetails'
    order_detail_id = db.Column(db.Integer, primary_key=True)
    order_id = db.Column(db.Integer, db.ForeignKey('orders.order_id'), nullable=False)
    product_id = db.Column(db.Integer, db.ForeignKey('products.product_id'), nullable=False)
    price = db.Column(db.Float, nullable=False)
    quantity = db.Column(db.Integer, nullable=False)
    discount = db.Column(db.Float, nullable=False)

    def to_dict(self):
        """
        Serialize order detail to dict, assume Product has to_dict method if needed.
        """
        return {
            'order_detail_id': self.order_detail_id,
            'product_id': self.product_id,
            'price': self.price,
            'quantity': self.quantity,
            'discount': self.discount
        }


# Fact inventory, holds stock information per product per branch
class Inventory(db.Model):
    __tablename__ = 'inventory'
    inventory_id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('products.product_id'))  # Foreign key to Products
    branch_id = db.Column(db.Integer, db.ForeignKey('branches.branch_id'))  # Foreign key to Branches
    quantity = db.Column(db.Integer)  # Quantity of the product available in the branch

    def to_dict(self):
        """
        Return a dictionary representation of an inventory item, including details of the
        product and branch, using joinedload for performance optimization.
        """
        # Fetch the inventory with related product and branch preloaded
        inventory = db.session.query(Inventory).options(
            joinedload(Inventory.product),
            joinedload(Inventory.branch)
        ).get(self.inventory_id)

        return {
            'inventory_id': inventory.inventory_id,
            'quantity': inventory.quantity,
            'product': {
                'product_id': inventory.product.product_id,
                'product_name': inventory.product.product_name,
                'product_description': inventory.product.product_desc,
                'product_price': inventory.product.product_price,
                'product_image': inventory.product.product_image
            } if inventory.product else None,
            'branch': {
                'branch_id': inventory.branch.branch_id,
                'branch_name': inventory.branch.branch_name,
                'branch_location': inventory.branch.branch_location,
                'branch_type': inventory.branch.branch_type
            } if inventory.branch else None
        }


# For logged-in users to manage shopping cart and active sessions.
class Sessions(db.Model):
    __tablename__ = 'sessions'
    session_id = db.Column(db.Integer, primary_key=True)
    # TBC
