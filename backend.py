from flask import Flask, jsonify, request, abort
from models import db, Employees, Customers, ProductGroup, Products, ProductSubGroup, Branches, Orders, OrderDetails, Inventory
from datetime import datetime
import os

# configuration
DB_NAME = 'stores.db'


def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = os.getenv('SECRET_KEY', 'your_secret_key_here')
    app.config["SQLALCHEMY_DATABASE_URI"] = f'sqlite:///{DB_NAME}'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)

    # Customer CRUD Operations

    @app.route('/customers', methods=['POST'])
    def create_customer():
        data = request.get_json()
        customer = Customers(
            username=data['username'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            email=data['email'],
            password=data['password']  # You might want to hash this password before storing
        )
        db.session.add(customer)
        db.session.commit()
        return jsonify(customer.to_dict()), 201

    @app.route('/customer/<int:customer_id>', methods=['GET'])
    def get_customer(customer_id):
        customer = Customers.query.get(customer_id)
        if customer:
            return jsonify(customer.to_dict())
        return jsonify({'message': 'User not found'}), 404

    @app.route('/customers', methods=['GET'])
    def get_customers():
        customers = Customers.query.all()
        if customers:
            return jsonify([customer.to_dict() for customer in customers])
        return jsonify({'message': 'Customer not found'}), 404

    @app.route('/employees', methods=['POST'])
    def create_employee():
        data = request.get_json()
        employee = Employees(
            username=data['username'],
            first_name=data['first_name'],
            last_name=data['last_name'],
            email=data['email'],
            password=data['password'],  # You might want to hash this password before storing
            is_admin=data.get('is_admin', 'cust')
        )
        db.session.add(employee)
        db.session.commit()
        return jsonify(employee.to_dict()), 201

    @app.route('/employee/<int:employee_id>', methods=['GET'])
    def get_employee(employee_id):
        employee = Employees.query.get(employee_id)
        if employee:
            return jsonify(employee.to_dict())
        return jsonify({'message': 'employee not found'}), 404

    @app.route('/employees', methods=['GET'])
    def get_employees():
        employees = Employees.query.all()
        if employees:
            return jsonify([employee.to_dict() for employee in employees])
        return jsonify({'message': 'employee not found'}), 404

    @app.route('/product_groups', methods=['GET'])
    def get_product_groups():
        product_groups = ProductGroup.query.all()
        if product_groups:
            return jsonify([product_group.to_dict() for product_group in product_groups])
        return jsonify({'message': 'No Product groups were found'}), 404

    @app.route('/users/<int:user_id>', methods=['PUT'])
    def update_user(user_id):
        user = Users.query.get(user_id)
        if user:
            data = request.get_json()
            user.username = data.get('username', user.username)
            user.first_name = data.get('first_name', user.first_name)
            user.last_name = data.get('last_name', user.last_name)
            user.email = data.get('email', user.email)
            user.role = data.get('role', user.role)
            db.session.commit()
            return jsonify(user.to_dict())
        return jsonify({'message': 'User not found'}), 404

    @app.route('/users/<int:user_id>', methods=['DELETE'])
    def delete_user(user_id):
        user = Users.query.get(user_id)
        if user:
            db.session.delete(user)
            db.session.commit()
            return jsonify({'message': 'User deleted'})
        return jsonify({'message': 'User not found'}), 404

    @app.route('/products', methods=['POST'])
    def create_product():
        data = request.get_json()
        product = Products(
            product_name=data['product_name'],
            product_desc=data['product_desc'],
            product_subgroup_id=data['product_subgroup_id'],
            product_image=data['product_image'],
            product_price=data['product_price']
        )
        db.session.add(product)
        db.session.commit()
        return jsonify(product.to_dict()), 201

    @app.route('/products/<int:product_id>', methods=['GET'])
    def get_product(product_id):
        product = Products.query.get(product_id)
        if product:
            return jsonify(product.to_dict())
        return jsonify({'message': 'Product not found'}), 404

    @app.route('/product_subgroups/<int:subgroup_id>', methods=['GET'])
    def get_product_subgroup(subgroup_id):
        product_subgroup = ProductSubGroup.query.get(subgroup_id)
        if product_subgroup:
            return jsonify(product_subgroup.to_dict())
        return jsonify({'message': 'Product subgroup not found'}), 404

    @app.route('/product_subgroups/<int:subgroup_id>', methods=['PUT'])
    def update_product_subgroup(subgroup_id):
        product_subgroup = ProductSubGroup.query.get(subgroup_id)
        if product_subgroup:
            data = request.get_json()
            product_subgroup.product_subgroup_name = data.get('product_subgroup_name',
                                                                  product_subgroup.product_subgroup_name)
            product_subgroup.product_subgroup_image = data.get('product_subgroup_image',
                                                                   product_subgroup.product_subgroup_image)
            db.session.commit()
            return jsonify(product_subgroup.to_dict())
        return jsonify({'message': 'Product subgroup not found'}), 404


    @app.route('/product_subgroups', methods=['POST'])
    def create_product_subgroup():
        data = request.get_json()
        product_subgroup = ProductSubGroup(
            product_subgroup_name=data['product_subgroup_name'],
            product_subgroup_image=data['product_subgroup_image'],
            product_group_id=data['product_group_id']
        )
        db.session.add(product_subgroup)
        db.session.commit()
        return jsonify(product_subgroup.to_dict()), 201

    @app.route('/product_subgroups/<int:subgroup_id>', methods=['DELETE'])
    def delete_product_subgroup(subgroup_id):
        product_subgroup = ProductSubGroup.query.get(subgroup_id)
        if product_subgroup:
            db.session.delete(product_subgroup)
            db.session.commit()
            return jsonify({'message': 'Product subgroup deleted'})
        return jsonify({'message': 'Product subgroup not found'}), 404

    @app.route('/product_groups', methods=['POST'])
    def create_product_group():
        data = request.get_json()
        product_group = ProductGroup(
            product_group_name=data['product_group_name'],
            product_group_image=data['product_group_image']
        )
        db.session.add(product_group)
        db.session.commit()
        return jsonify(product_group.to_dict()), 201

    @app.route('/product_groups/<int:group_id>', methods=['GET'])
    def get_product_group(group_id):
        product_group = ProductGroup.query.get(group_id)
        if product_group:
            return jsonify(product_group.to_dict())
        return jsonify({'message': 'Product group not found'}), 404

    @app.route('/product_groups/<int:group_id>', methods=['PUT'])
    def update_product_group(group_id):
        product_group = ProductGroup.query.get(group_id)
        if product_group:
            data = request.get_json()
            product_group.product_group_name = data.get('product_group_name', product_group.product_group_name)
            product_group.product_group_image = data.get('product_group_image', product_group.product_group_image)
            db.session.commit()
            return jsonify(product_group.to_dict())
        return jsonify({'message': 'Product group not found'}), 404

    @app.route('/product_groups/<int:group_id>', methods=['DELETE'])
    def delete_product_group(group_id):
        product_group = ProductGroup.query.get(group_id)
        if product_group:
            db.session.delete(product_group)
            db.session.commit()
            return jsonify({'message': 'Product group deleted'})
        return jsonify({'message': 'Product group not found'}), 404

    @app.route('/branches', methods=['POST'])
    def create_branch():
        data = request.get_json()
        branch = Branches(
            branch_name=data['branch_name'],
            branch_location=data['branch_location'],
            branch_type=data['branch_type']
        )
        db.session.add(branch)
        db.session.commit()
        return jsonify(branch.to_dict()), 201

    @app.route('/branches/<int:branch_id>', methods=['GET'])
    def get_branch(branch_id):
        branch = Branches.query.get(branch_id)
        if branch:
            return jsonify(branch.to_dict())
        return jsonify({'message': 'Branch not found'}), 404

    @app.route('/branches/<int:branch_id>', methods=['PUT'])
    def update_branch(branch_id):
        branch = Branches.query.get(branch_id)
        if branch:
            data = request.get_json()
            branch.branch_name = data.get('branch_name', branch.branch_name)
            branch.branch_location = data.get('branch_location', branch.branch_location)
            branch.branch_type = data.get('branch_type', branch.branch_type)
            db.session.commit()
            return jsonify(branch.to_dict())
        return jsonify({'message': 'Branch not found'}), 404

    @app.route('/branches/<int:branch_id>', methods=['DELETE'])
    def delete_branch(branch_id):
        branch = Branches.query.get(branch_id)
        if branch:
            db.session.delete(branch)
            db.session.commit()
            return jsonify({'message': 'Branch deleted'})
        return jsonify({'message': 'Branch not found'}), 404

    @app.route('/orders', methods=['POST'])
    def create_order():
        data = request.get_json()
        order = Orders(
            order_date=datetime.utcnow(),
            branch_id=data['branch_id'],
            user_id=data['user_id'],
            status_id=data['status_id']
        )
        db.session.add(order)
        db.session.commit()
        return jsonify(order.to_dict()), 201

    @app.route('/orders/<int:order_id>', methods=['GET'])
    def get_order(order_id):
        order = Orders.query.get(order_id)
        if order:
            return jsonify(order.to_dict())
        return jsonify({'message': 'Order not found'}), 404

    @app.route('/orders/<int:order_id>', methods=['PUT'])
    def update_order(order_id):
        order = Orders.query.get(order_id)
        if order:
            data = request.get_json()
            order.branch_id = data.get('branch_id', order.branch_id)
            order.user_id = data.get('user_id', order.user_id)
            order.status_id = data.get('status_id', order.status_id)
            db.session.commit()
            return jsonify(order.to_dict())
        return jsonify({'message': 'Order not found'}), 404

    @app.route('/orders/<int:order_id>', methods=['DELETE'])
    def delete_order(order_id):
        order = Orders.query.get(order_id)
        if order:
            db.session.delete(order)
            db.session.commit()
            return jsonify({'message': 'Order deleted'})
        return jsonify({'message': 'Order not found'}), 404

    @app.route('/order_details', methods=['POST'])
    def create_order_detail():
        data = request.get_json()
        order_detail = OrderDetails(
            order_id=data['order_id'],
            product_id=data['product_id'],
            price=data['price'],
            quantity=data['quantity'],
            discount=data['discount']
        )
        db.session.add(order_detail)
        db.session.commit()
        return jsonify(order_detail.to_dict()), 201

    @app.route('/order_details/<int:detail_id>', methods=['GET'])
    def get_order_detail(detail_id):
        order_detail = OrderDetails.query.get(detail_id)
        if order_detail:
            return jsonify(order_detail.to_dict())
        return jsonify({'message': 'Order detail not found'}), 404

    @app.route('/order_details/<int:detail_id>', methods=['PUT'])
    def update_order_detail(detail_id):
        order_detail = OrderDetails.query.get(detail_id)
        if order_detail:
            data = request.get_json()
            order_detail.price = data.get('price', order_detail.price)
            order_detail.quantity = data.get('quantity', order_detail.quantity)
            order_detail.discount = data.get('discount', order_detail.discount)
            db.session.commit()
            return jsonify(order_detail.to_dict())
        return jsonify({'message': 'Order detail not found'}), 404

    @app.route('/order_details/<int:detail_id>', methods=['DELETE'])
    def delete_order_detail(detail_id):
        order_detail = OrderDetails.query.get(detail_id)
        if order_detail:
            db.session.delete(order_detail)
            db.session.commit()
            return jsonify({'message': 'Order detail deleted'})
        return jsonify({'message': 'Order detail not found'}), 404

    @app.route('/inventory', methods=['POST'])
    def create_inventory():
        data = request.get_json()
        inventory = Inventory(
            product_id=data['product_id'],
            branch_id=data['branch_id'],
            quantity=data['quantity']
        )
        db.session.add(inventory)
        db.session.commit()
        return jsonify(inventory.to_dict()), 201

    @app.route('/inventory/<int:inventory_id>', methods=['GET'])
    def get_inventory(inventory_id):
        inventory = Inventory.query.get(inventory_id)
        if inventory:
            return jsonify(inventory.to_dict())
        return jsonify({'message': 'Inventory not found'}), 404

    @app.route('/inventory/<int:inventory_id>', methods=['PUT'])
    def update_inventory(inventory_id):
        inventory = Inventory.query.get(inventory_id)
        if inventory:
            data = request.get_json()
            inventory.quantity = data.get('quantity', inventory.quantity)
            db.session.commit()
            return jsonify(inventory.to_dict())
        return jsonify({'message': 'Inventory not found'}), 404

    @app.route('/inventory/<int:inventory_id>', methods=['DELETE'])
    def delete_inventory(inventory_id):
        inventory = Inventory.query.get(inventory_id)
        if inventory:
            db.session.delete(inventory)
            db.session.commit()
            return jsonify({'message': 'Inventory deleted'})
        return jsonify({'message': 'Inventory not found'}), 404

    return app


if __name__ == '__main__':
    app = create_app()

    with app.app_context():
        db.create_all()  # Create tables based on the models
    app.run(debug=True)

