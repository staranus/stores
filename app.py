from flask import Flask, render_template



import requests

app = Flask(__name__)

backend_url = "http://localhost:5000"


@app.route('/')
def home():
    response = requests.get(f"{backend_url}/product_groups")
    product_groups = response.json() if response.status_code == 200 else []
    return render_template('index.html', product_groups=product_groups)


@app.route('/category/<int:group_id>')
def category(group_id):
    response = requests.get(f"{backend_url}/product_groups/{group_id}")
    if response.status_code == 200:
        category = response.json()
        return render_template('category.html', category=category)
    else:
        # Handle error, for example, return a 404 page
        return render_template('404.html'), 404


@app.route('/product/<int:product_id>')
def product_details(product_id):
    response = requests.get(f"{backend_url}/products/{product_id}")
    product = response.json() if response.status_code == 200 else {}
    return render_template('product_details.html', product=product)


if __name__ == '__main__':
    app.run(port=8000)  # Ensure different port if backend is also running on Flask
